//
//  ViewController.swift
//  calveraTestSwift
//
//  Created by book on 20.03.16.
//
//

import UIKit



class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var content = [NewsItemModel]()
    
    
    var isSorted :Bool = false
    struct Storyboard {
        static let kPublicationCellId = "publicationCell"
    }
    
    
    
    internal var refreshControl: UIRefreshControl?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refresh()
        //self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
        
        self.tableView.registerNib(UINib(nibName: "PublicationTableViewCell", bundle: nil), forCellReuseIdentifier: Storyboard.kPublicationCellId)
        
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        tableView!.addSubview(self.refreshControl!)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh() {
        
        ServerManager().getNewsFromServer { (result: [NewsItemModel]) -> () in
            self.content = result
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
        self.refreshControl?.endRefreshing()
        
    }
    
    
    @IBAction func sortButtonAction(sender: AnyObject) {
        
        var sortedResults = [NewsItemModel]()
        sortedResults = content
        if (isSorted) {
            isSorted = false;
            sortedResults.sortInPlace({$0.posted_time.doubleValue > $1.posted_time.doubleValue})
            
        } else {
            isSorted = true;
            sortedResults.sortInPlace({$0.posted_time.doubleValue < $1.posted_time.doubleValue})
        }
        content = sortedResults
        tableView.reloadData()
        
        
    }
    
    //pragma mark - UITableviewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection: Int ) -> Int {
        
        return content.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Storyboard.kPublicationCellId, forIndexPath: indexPath) as! PublicationTableViewCell
        self.configCell(cell, indexPath: indexPath)
        
        return cell
        
    }
    
    func configCell(cell:PublicationTableViewCell, indexPath:NSIndexPath){
        
        let item = content[indexPath.row]
        
        if (item.category_id.isEqual(208) ) {
            
            cell.categoryLabel.text = "Футбол"
            
        } else {
            
            cell.categoryLabel.text = ""
            
        }
        
        if (item.main.isEqual(1)) {
            cell.publicationTitleLabel.font = UIFont .boldSystemFontOfSize(17)
        }
        
        let formatter = NSDateFormatter()
        
        let date = NSDate(timeIntervalSince1970:item.posted_time as Double )
        
        formatter.dateFormat = "HH:mm"
        cell.timeAgoLabel.text =  formatter.stringFromDate(date)
        
        let stringTitle = NSMutableAttributedString(string: item.title as String)
        let textAttachment = NSTextAttachment()
        textAttachment.image = UIImage(named: "comments_badge")
        let imageString = NSAttributedString(attachment: textAttachment)
        stringTitle.appendAttributedString(NSMutableAttributedString(string:"  "))
        stringTitle.appendAttributedString(imageString)
        let countString =  NSMutableAttributedString(string: item.comment_count .stringValue )
        stringTitle.appendAttributedString(NSMutableAttributedString(string:"  "))
        stringTitle.appendAttributedString(countString)
        
        cell.publicationTitleLabel.attributedText = stringTitle
        
    }
}
