//
//  ServerManager.swift
//  calveraTestSwift
//
//  Created by book on 22.03.16.
//
//

import Foundation
import Alamofire
import SwiftyJSON

class ServerManager {
    func getNewsFromServer(callback: ([NewsItemModel]) -> Void) {
        
        Alamofire.request(.GET, "http://calvera.su/5839.json")
            .responseString { response in
                print("Success: \(response.result.isSuccess)")
                let rawString = NSString(data: response.data!, encoding: NSWindowsCP1251StringEncoding)!
                let clearString :NSString = rawString.stringByConvertingHTMLToPlainText()
                let data = clearString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
                let str = String(data: data!, encoding: NSUTF8StringEncoding)
                self.fetchNewsFromString(str,callback: callback)
                
        }
        
    }
    
    func fetchNewsFromString(data: String!,callback: ([NewsItemModel]) -> Void)  {
        
        var arrayItems = [NewsItemModel]()
        if let dataFromString = data.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
            let json = JSON(data: dataFromString)
            
            for item in json["news"].arrayValue {
                
                var newsItem = NewsItemModel()
                newsItem.initWithServerResponse(item)
                
                
                if(newsItem.category_id .isEqual(208) ) {
                    arrayItems .append(newsItem)
                }
            }
            callback(arrayItems)
        }
        
    }
    
}
