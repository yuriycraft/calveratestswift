//
//  NewsItemModel.swift
//  calveraTestSwift
//
//  Created by book on 20.03.16.
//
//

import Foundation
import SwiftyJSON

class NewsItemModel {
    
    
    var category_id:NSNumber!
    var comment_count:NSNumber!
    var main:NSNumber!
    var posted_time:NSNumber!
    var title:NSString!
    
    
    func initWithServerResponse(item: JSON) -> Void {
        
        
        posted_time  =  item["posted_time"].numberValue
        category_id = item["category_id"].numberValue
        comment_count = item["comment_count"].numberValue
        main = item["main"].boolValue
        title = item["title"].stringValue
        
    }


}
