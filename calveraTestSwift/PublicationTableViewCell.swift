//
//  PublicationTableViewCell.swift
//  calveraTestSwift
//
//  Created by book on 22.03.16.
//
//

import UIKit

class PublicationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var publicationTitleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var timeAgoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
